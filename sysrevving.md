--- 
title: "Sys Revving"
author: "Gjalt-Jorn Peters"
date: "10:29:22 on 2021-10-30 CEST (UTC+0200)"
site: bookdown::bookdown_site
documentclass: book
bibliography: [book.bib, packages.bib]
url: https://sysrevver.com
# cover-image: path to the social sharing image like images/cover.jpg
description: |
  The Sys Revving book is an open access book about doing open and systematic
  systematic reviews using open source tools.
biblio-style: apalike
csl: chicago-fullnote-bibliography.csl
---
--- 
title: "Sys Revving"
author: "Gjalt-Jorn Peters"
date: "10:29:22 on 2021-10-30 CEST (UTC+0200)"
site: bookdown::bookdown_site
documentclass: book
bibliography: [book.bib, packages.bib]
url: https://sysrevver.com
# cover-image: path to the social sharing image like images/cover.jpg
description: |
  The Sys Revving book is an open access book about doing open and systematic
  systematic reviews using open source tools.
biblio-style: apalike
csl: chicago-fullnote-bibliography.csl
---

# About

The Sys Revving book is an open access book about doing open and systematic systematic reviews using open source tools. That is not a typo: it really says systematic systematic reviews, reflecting how this book aims to make systematic reviews as systematic as possible. This is tied in with the aim of making them as open, transparent, and machine-readable as possible.

The workflow adopted for many systematic reviews is often systematic but only for humans, and often in a way specific to a given reviews or a given lab. The documentation is often scattered over multiple files, sometimes even in proprietary file formats or formats used by free but not free/libre open source software (FLOSS). This book aims to support a systematic workflow geared towards optimizing transparency and machine-readability.



<!--chapter:end:index.Rmd-->

# Types of reviews

Scoping reviews, meta-analyses, systematic reviews, narrative reviews, conceptual reviews.

<!--chapter:end:types-of-reviews.Rmd-->


# The Categorization-Coding Continuum {#categorization-coding-continuum}

Placeholder


### Categorization
### Ambiguity
### The cost of categorization
### Coding after extraction
### The categorization-coding continuum

<!--chapter:end:categorization-coding-continuum.Rmd-->


# References {-}


<!--chapter:end:references.Rmd-->

