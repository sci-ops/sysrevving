# SysRevving

You can read this book at https://sysrevving.com (or, if you prefer, you can download [the PDF version](https://sysrevving.com/sysrevving.pdf) or [the Ebook version (.epub)](https://sysrevving.com/sysrevving.epub)).

The SysRevving book is an open access book about doing open and systematic systematic reviews using open source tools. That is not a typo: it really says systematic systematic reviews, reflecting how this book aims to make systematic reviews as systematic as possible. This is tied in with the aim of making them as open, transparent, and machine-readable as possible.

The scientific literature is rapidly expanding, and keeping up-to-date by reading new journal issues as they come out has been impossible for many years already. As a consequence, two practices are becoming increasingly common: PhD. candidates starting their project with a systematic review to get a hopefully unbiased overview of the literature in a field, and conducting living systematic reviews that can be updated with relatively little effort to provide an up-to-date database to answer specific questions.

At the same time, the workflow adopted in many systematic reviews is often partly designed anew for each systematic review. Decisions, procedures, and documentation are often only human-readable and scattered over multiple files, sometimes even in proprietary file formats or formats used by free but not free/libre open source software (FLOSS). This book aims to support a systematic workflow geared towards optimizing transparency and machine-readability.

The idea is that optimizing transparency and machine-readability means that systematic reviews can ultimately become more scalable, interoperable, and extensible. This in turn means that there is less wasted effort and eventually, systematic reviews can be accelerated ('revved up', so to speak).
