# Executing the Screening {#sec-execution-screening}

Screening is the process of evaluating every result of the search strategy and applying the exclusion criteria.^[Inclusion criteria can never override an exclusion criterion; therefore, inclusion of a soure is implicit in it not being excluded during screening.] 


## Prepare JabRef for screening

To prepare JabRef for screening, you need to mask a number of fields from the screeners, specifically any fields that may contain information that might bias the screeners. Commonly masked fields are authors, journal, and publication year. There are two places where screeners may be exposed to this information: in the entry table (the overview of all bibliographic entries in the database) and in the entry editor (where the screeners enter their screening decision).

You can change both settings in the JabRef preferences in the Options menu. To specify which fields should be visible in the entry table, open the "Entry table" section of the preferences dialog as shown in @fig-jabref1. You can specify custom columns at the bottom of the overview. In this example, we add custom field `screener_id1_stage_1`, which specifies that these decisions are made by the screener with identifier `id1` in screening stage 1 (often in stage 1, only the titles are screened).

![A screenshot of the JabRef preferences showing the 'Entry table' section.](img/planning-screening---jabref--preferences--entry-table.png){#fig-jabref1 fig-alt="A screenshot of the JabRef preferences showing the 'Entry table' section."}

You can set the fields that screeners are exposed to in the "Custom editor tabs" section. Here, you can make tabs for each screening stage. For example, you may want to include only the title in the tab for stage 1, but also the abstract in the tab for stage 2. In additon, you'll want to include the field with the decisions for the relevant stage. For example, in if you enter a row containing "`Screening Stage 1:title;duplicate;screener_id1_stage_1`", that will add a tab called "Screening Stage 1", showing the fields "title", "duplicate", and "screener_id1_stage_1". If you add a second row containing "`Screening Stage 2:title;abstract;duplicate;screener_id1_stage_2`", that editor tab will also show the abstract, and instead of entering the decision in field `screener_id1_stage_1`, it is entered in field `screener_id1_stage_2` (i.e. the decision for screener with identifier `id` in stage 2). These rows have been entered in the example in @fig-jabref2.

![A screenshot of the JabRef preferences showing the 'Custom editor tabs' section.](img/planning-screening---jabref--preferences--custom-editor-tabs.png){#fig-jabref2 fig-alt="A screenshot of the JabRef preferences showing the 'Custom editor tabs' section."}

## Screening in JabRef

To start screening, open the first bibliographic entry at the top of the entry table. Then, open the editor tab corresponding to the stage you're screening in.

Read fields; apply criteria; type in identifier for selected exclusion criterion or inclusion; open next entry, etc.

👉 TODO: find shortcut key for moving through entries while staying in the 'screening' field of the entry editor.

Options -> Key bindings -> default, alt-down and alt-up. But this seems to unfocus the editor?
